FROM python
RUN apt update; apt install -y libmemcached-dev musl-dev libffi-dev libmemcached-dev ffmpeg bc imagemagick;
COPY requirments.txt .
RUN pip install -r requirments.txt
ENV PYTHONPATH=/app/GLaDOS
WORKDIR /app/GLaDOS/vk_bot/BotFiles
CMD python /app/GLaDOS/vk_bot/main2.py
COPY vk_bot/ /app/GLaDOS/vk_bot/

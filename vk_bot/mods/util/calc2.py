"""
from vk_bot.core.modules.basicplug import BasicPlug
import re


class Calculator2(BasicPlug):
    doc = "Полноценный калькулятор"
    command = ("калькулятор", "посчитай", "calc", "скока",)
    include=False

    def main(self):
        equation = "".join(self.text[1:])
        try:
            if equation.find("**") > -1:
                raise Exception()
            int(re.sub(r"[()+*-/e]", "", equation))
            self.sendmsg(f"Результат: {eval(equation)}")
        except ValueError:
            self.sendmsg("Использовать в уравнениях можно только +*-/()e")
            return
        except (ZeroDivisionError, Exception):
            self.sendmsg("Планету взорвать хочешь?")
"""

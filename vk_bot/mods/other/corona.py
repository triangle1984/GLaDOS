from vk_bot.core.modules.basicplug import BasicPlug
import requests


class coronavirus(BasicPlug):
    command = ("коронавирус", "корона")
    doc = "Узнать число зараженных"

    def main(self):
        res = requests.get(
            "https://coronavirus-tracker-api.herokuapp.com/v2/locations").json()['locations']
        for coun in res:
            if coun["country"].lower() == self.text[1].lower():
                virus = coun["latest"]
                self.sendmsg(f"""
Заражено: {virus['confirmed']}
Умерло: {virus['deaths']}
""")
                return

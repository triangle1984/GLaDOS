from vk_bot.core.modules.basicplug import BasicPlug
import subprocess
import re


class BC(BasicPlug):
    command = ("калькулятор", "бс", "бц", "bc", "скока")
    doc = "калькулятор использующий GNU/bc вместо eval()"

    def main(self):
        problem = "".join(self.text[1:]) + "\n"
        try:
            int(re.sub(r"[()+*-/^]", "", problem))
        except ValueError:
            self.sendmsg("!error")
            return
        bc = subprocess.run(["bc", "-l"], stdout=subprocess.PIPE,
                            input=problem.encode("utf-8"))
        self.sendmsg("Результат: " + bc.stdout.decode("utf-8"))
